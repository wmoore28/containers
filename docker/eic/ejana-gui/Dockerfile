#--------------------------------------------------------------------------
# electronioncollider/ejana-gui
#
# This Dockerfile describes a shared base container image for EIC Software
# Consortium images with GEANT4 data added, and full graphics support.
#
# This will create a docker image that includes
# both a noVNC-webserver and VNC-server that can be used to view
# graphics on the host using either a native webrowser (HTML5 enabled)
# or a native VNC viewer. This includes the ability to view OpenGL
# graphics.
#
# This was originally inspired by the Docker-opengl project here:
#
#  https://github.com/thewtex/docker-opengl
#
# In the end, the only things used from that were the ideas to use
# noVNC and openbox. Thus, we do not include their license. However,
# I do want to give them credit for inspiring this work.
#
#
# Run this with:
#
#  docker run -it --rm -p 6080:6080 -p 5901:5901 electronioncollider/ejana-gui
#
#
#--------------------------------------------------------------------------
#
# Image size: 5.71GB
#
#   docker build -t ejana-gui .
#
#   docker tag ejana-gui electronioncollider/ejana-gui:latest
#
#   docker push electronioncollider/ejana-gui:latest
#
#--------------------------------------------------------------------------

FROM electronioncollider/ejana-app:latest
LABEL maintainer "David Lawrence <davidl@jlab.org>"

USER root

#----------------------------------------------------
# VNC and noVNC servers
#
# Note: for some unknown reason I was suddenly getting
# certificate errors when doing yum install here. The
# workaround is to globally disable certificate checks
# (the first line). This wasn't happening earlier on
# the base containers (??)
#
RUN    yum-config-manager --save --setopt=sslverify=false \
	&& yum install -y epel-release 

RUN yum install -y \
		xfce4-session \
		xfce4-panel \
		xfwm4 \
		xfce4-terminal \
		tigervnc-server \
		ghostscript-fonts \
		xorg-x11-fonts-misc \
		net-tools \
		glx-utils \
		python-websockify \
		xterm \
		nedit \
		psmisc \
		git \
		which \
		tcsh \
	&& git clone https://github.com/kanaka/noVNC.git /opt/noVNC \
	&& cd /opt/noVNC \
	&& ln -s vnc.html index.html \
	&& mkdir -p /container/utilities

# These define an alias and a script for easily starting up the
# servers from either bash or tcsh.
COPY xstart.sh /etc/profile.d
COPY xstart.csh /etc/profile.d
COPY eic_xstart.sh /container/utilities
COPY eic_xstart.csh /container/utilities
COPY xstop /usr/bin
COPY README.md /home/eicuser
ADD dot_config /home/eicuser/.config
ADD dot_Xclients /home/eicuser/.Xclients

#----------------------------------------------------
# dsh
#
# n.b. a version of dsh was copied to the gitlab eic/containers
# repository. We still use the version from ESC/containers for
# now. At some point, one of them needs to be removed
#
RUN cd $EIC_ROOT/app \
	&& git  -c http.sslVerify=false clone https://gitlab.com/ESC/containers.git tmp_containers \
	&& mv tmp_containers/Docker/utilities/dsh $EIC_ROOT/app \
	&& rm -rf tmp_containers

RUN mkdir -p ${CONTAINER_ROOT}/ejana-gui
COPY Dockerfile ${CONTAINER_ROOT}/ejana-gui
COPY xstart.sh ${CONTAINER_ROOT}/ejana-gui
COPY xstart.csh ${CONTAINER_ROOT}/ejana-gui
COPY xstop ${CONTAINER_ROOT}/ejana-gui
COPY eic_xstart.sh ${CONTAINER_ROOT}/ejana-gui
COPY eic_xstart.csh ${CONTAINER_ROOT}/ejana-gui
COPY README.md ${CONTAINER_ROOT}/ejana-gui
COPY dot_Xclients ${CONTAINER_ROOT}/ejana-gui
COPY dot_config ${CONTAINER_ROOT}/ejana-gui/dot_config
COPY dot_bash_history ${CONTAINER_ROOT}/ejana-gui


# The following doesn't seem to work as expected. One still
# needs to run the container with -p 6080:6080 or 
# -p 5900:5900 . I'll leave this here for now since it doesn't hurt.
EXPOSE 6080 5900

USER eicuser

WORKDIR /home/eicuser
CMD bash /container/utilities/eic_xstart.sh && sleep infinity
