path_to_file = "/usr/local/share/jupyter/lab/static/index.html"
# Read in the file
with open(path_to_file, 'r') as file :
  filedata = file.read()

# Replace the target string
filedata = filedata.replace('</body>', '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.6/require.min.js"</script></body>')

# Write the file out again
with open(path_to_file, 'w') as file:
  file.write(filedata)