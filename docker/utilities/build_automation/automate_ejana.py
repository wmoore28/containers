# Some useful links:#    https://docker-py.readthedocs.io/en/stable/images.html
import inspect
import os
from os import path
import argparse
from automation import DockerAutomation, ImageInfo as Image

# This script default location is:
#    containers/docker/utilities/ejana_build_automation
# We need:
#    containers/docker/devops
#    containter/docker/eic

this_path = path.dirname(path.abspath(inspect.stack()[0][1]))
devops_root_path = path.join(this_path,'../../devops')
eic_root_path = path.join(this_path, '../../eic')


# List of images (and its dependencies)
images = {
    'devops': [
        Image('ejana-centos7-prereq', build_flags='--squash'),  # doesn't depends on images from this collection
        Image('g4e-centos7-prereq', depends_on='ejana-centos7-prereq'),
        Image('g4e-centos7', depends_on='g4e-centos7-prereq'),
        Image('ejana-ubuntu18-prereq')],
    'eic': [
        Image('eic-base'),
        Image('eic-science', depends_on='eic-base'),
        Image('ejana-app', depends_on='eic-science'),
        Image('ejana-gui', depends_on='eic-app')
    ]
}
images['all'] = images['eic'] + images['devops']

images_by_name = {image.name: image for image in images['all']}


def set_images_params(images, root_path, organisation, version):
    for image in images:
        image.tag = f'{organisation}/{image.name}:{version}'
        image.path = f"{root_path}/{image.name}"

def print_images():
    for image_set_name, image_set in images.items():
        print(image_set_name)
        for image in image_set:
            print(f'   {image}')

def main():
    cwd = os.getcwd()
    parser = argparse.ArgumentParser()
    parser.add_argument("--no-cache", help="Use docker --no-cache flag during build", action="store_true")
    parser.add_argument("--push", type=bool, help="If true - push images if built successfully. (true by default)", default=False)
    parser.add_argument("--check-deps", type=bool, help="Check that dependency is built", default=True)
    parser.add_argument("command", type=str, help="'list' = list known images. 'devops', 'eic', 'all' = build set of images. exact image name = build that image")
    args = parser.parse_args()

    set_images_params(images['devops'], devops_root_path, 'eicdev', 'latest')
    set_images_params(images['eic'], eic_root_path, 'electronioncollider', 'latest')

    if args.command == 'list':
        print_images()
        exit(0)

    if args.command in ['devops', 'eic', 'all']:
        single_image_build = False
        automation = DockerAutomation(images[args.command])
    else:
        single_image_build = True
        automation = DockerAutomation(images['all'])
        automation.check_deps = False

    automation.no_cache = args.no_cache
    automation.push_after_build = args.push

    if single_image_build:
        automation.build(args.command)
    else:
        automation.build_all()

    logs = automation.operation_logs

    os.chdir(cwd)

    print('SUMMARY:')
    print("{:<8} {:<22} {:<9} {:<11} {:<21} {:<21}"
          .format('ACTION', 'IMAGE NAME', 'RETCODE', 'DURATION', 'START TIME', 'END TIME'))
    for log in logs:
        print("{op:<8} {name:<22} {ret_code:<9} {duration_str:<11} {start_time_str:<21} {end_time_str:<21}".format(**log))

    #import json
    #with open('result.json', 'w') as outfile:
    #    json.dump(logs, outfile, indent=4, ensure_ascii=False)


if __name__ == '__main__':
    main()

