import inspect
import json
import os
import shlex
import subprocess
from datetime import datetime
from typing import Union, List

default_root_path = os.path.dirname(os.path.abspath(inspect.stack()[0][1]))


class ImageInfo:
    def __init__(self, name: str, depends_on:str = '', build_flags:str = ''):
        self.name = name
        self.depends_on = depends_on
        self.tag = ''       # unknown yet. Set by build process
        self.path = ''      # unknown yet. Set by build process

    def __repr__(self):
        return self.name



def _run(command: Union[str, list]) -> (int, datetime, datetime, list):
    """Wrapper around subprocess.Popen that returns:


    :return retval, start_time, end_time, lines

    """
    if isinstance(command, str):
        command = shlex.split(command)

    # Pretty header for the command
    pretty_header = "RUN: " + " ".join(command)
    print('=' * len(pretty_header))
    print(pretty_header)
    print('=' * len(pretty_header))

    # Record the start time
    start_time = datetime.now()
    lines = []

    # stderr is redirected to STDOUT because otherwise it needs special handling
    # we don't need it and we don't care as C++ warnings generate too much stderr
    # which makes it pretty much like stdout
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    while True:
        output = process.stdout.readline().decode('latin-1')

        if process.poll() is not None and output == '':
            break
        if output:
            print(output, end="")
            lines.append(output)

    # Get return value and finishing time
    retval = process.poll()
    end_time = datetime.now()

    print("------------------------------------------")
    print(f"RUN DONE. RETVAL: {retval} \n\n")

    return retval, start_time, end_time, lines


class DockerAutomation(object):

    def __init__(self, images: List[ImageInfo]):  # like "ejana-centos7-prereq"
        self.operation_logs: List[dict] = []
        self.images = images
        self._images_by_name = {image.name: image for image in self.images}
        self.built_images_by_name = {}
        self.check_deps = True
        self.no_cache = False
        self.push_after_build = True

    def _append_log(self, op, name, ret_code, start_time, end_time, output):
        """Saves data to specially formatted record"""
        duration = end_time - start_time
        self.operation_logs.append({'op':op,
                                    'name': name,
                                    'ret_code': ret_code,
                                    'start_time': start_time,
                                    'start_time_str': start_time.strftime("%Y-%m-%d %H:%M:%S"),
                                    'end_time': end_time,
                                    'end_time_str': end_time.strftime("%Y-%m-%d %H:%M:%S"),
                                    'duration': duration,
                                    'duration_str': str(duration)[:9],
                                    'output': output})

    def _build_image(self, image: ImageInfo):
        """
            docker build --tag=ejana-centos7-prereq .
            docker tag ejana-centos7-prereq eicdev/ejana-centos7-prereq:latest
            docker push eicdev/ejana-centos7-prereq:latest
        """
        # Check if we built dependency
        if self.check_deps:
            if image.depends_on and image.depends_on not in self.built_images_by_name.keys():
                message = f"The image {image.name} depends on {image.depends_on} which has not been built. " \
                          f"Run this script with check-deps=false in order to force {image.name} building"
                print(message)

                # Save it to the logs
                self._append_log('check', image.name, 1, datetime.now(), datetime.now(), [message])                
                return

        # no-cache flag given?
        no_cache_str = "--no-cache" if self.no_cache else ""

        # RUN DOCKER BUILD COMMAND
        os.chdir(image.path)
        retval, start_time, end_time, output = _run(f"docker build {no_cache_str} --tag={image.tag} .")

        # Log the results:
        self._append_log('build', image.name, retval, start_time, end_time, output)

        if retval:
            print(f"(! ! !)   ERROR   (! ! !) build op return code is: {retval}")
        else:
            self.built_images_by_name[image.name] = image
            if self.push_after_build:
                self.push(image)

    def build(self, image_name: str):
        self._build_image(self._images_by_name[image_name])

    def build_all(self):
        for image in self.images:
            self._build_image(image)

    def push(self, name_or_image):
        if isinstance(name_or_image, ImageInfo):
            image = name_or_image
        else:
            image = self._images_by_name[name_or_image]
        os.chdir(image.path)
        retval, start_time, end_time, output = _run(f"docker push {image.tag}")

        # Log the results:
        self._append_log('push', image.name, retval, start_time, end_time, output)       

        if retval:
            print(f"(! ! !)   ERROR   (! ! !) PUSH operation return code is: {retval}")

    def push_built(self):
        for name in self.built_images_by_name.keys():
            self.push(name)

    def push_all(self):
        for name in self._images_by_name.keys():
            self.push(name)

